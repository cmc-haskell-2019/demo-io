{-# LANGUAGE RecordWildCards #-}
module DemoIO
    ( run
    ) where

import System.IO
import System.Random

import Graphics.Gloss.Interface.Pure.Game

--------------
-- Data types.
--------------

-- Config for colors.
data Config = Config
  { cfgColor1    :: Color -- Color for first number.
  , cfgColor2    :: Color -- Color for second number.
  }

-- Game state.
data GameState = GameState
  { gameNumber    :: Int    -- Current number.
  , gameRandomGen :: StdGen -- Random number generator.
  , gameConfig    :: Config -- Colors config.
  }

-------------
-- Constants.
-------------

-- Path to confi file.
configPath :: FilePath
configPath = "config.txt"

-- Game display mode.
display :: Display
display = FullScreen

-- Background color.
bgColor :: Color
bgColor = greyN 0.3

-- Simulation steps per second.
stepsPerSecond :: Int
stepsPerSecond = 60

-- Text shift on screen.
textShift :: Float
textShift = 300

-- Random numbers range.
range :: (Int, Int)
range = (-10, 10)

------------------
-- Pure functions.
------------------

-- Parse config from string.
-- Config format: 2 lines, one color per line.
parseConfig :: String -> Maybe Config
parseConfig str = case map readColor (lines str) of
  [Just c1, Just c2] -> Just (Config c1 c2)
  _ -> Nothing
  where
    readColor :: String -> Maybe Color
    readColor s = lookup s (zip names colors)
    colors = [red, green, blue, black]
    names = ["red", "green", "blue", "black"]

-- Draw a picture: two numbers of different colors defined in config.
drawGame :: GameState -> Picture
drawGame (GameState n _ Config{..}) = Pictures [leftHalf, rightHalf]
  where
    numText = Text (show n)
    leftHalf = Translate (-textShift) 0 $ Color cfgColor1 numText
    rightHalf = Translate textShift 0 $ Color cfgColor2 numText

-- Handle events.
handleEvent :: Event -> GameState -> GameState
-- Generate new random number when Enter is pressed.
handleEvent (EventKey (SpecialKey KeyEnter) Down _ _) game@GameState{..} =
  -- Get new random number and generator.
  let (n, newGen) = randomR range gameRandomGen
  -- Update BOTH number AND generator.
  in game { gameNumber = n, gameRandomGen = newGen }
-- Ignore all other events.
handleEvent _ g = g

-- Simulation step (updates nothing).
update :: Float -> GameState -> GameState
update _ = id

------------------------------
-- Main function for this app.
------------------------------

-- Run game. This is the ONLY unpure function.
run :: IO ()
run = do
  -- Load config file contents (unpure action).
  fileContents <- readFile configPath
  -- Try to parse config.
  case parseConfig fileContents of
    Nothing  -> putStrLn "Cannot parse config" -- Exit with error.
    Just cfg -> do
      -- Get new random number generator (unpure action).
      gen <- newStdGen
      let initState = GameState 0 gen cfg
      play display bgColor stepsPerSecond initState drawGame handleEvent update
